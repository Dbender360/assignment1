Build and install:

1. Ensure internet browser is installed on device (Chrome, Firefox, Microsoft Edge, etc.).
2. Ensure you have an internet connection enabled on your device.
3. Download project html file and locate in file explorer.
4. Right click on file and choose "Open with", selecting your local internet browser.
5. Page should open without problems.

License:

Creative Commons, https://creativecommons.org/licenses/by/4.0/

I chose a public license like this because I have no issues with this code being used for 
anything that benefits someone else and not myself, and therefore have no problem making 
it publicly accessible for others to use.